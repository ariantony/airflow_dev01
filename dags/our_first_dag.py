from datetime import datetime, timedelta
from airflow import DAG
from airflow.operator.bash

default_args = {
    'owner': 'Tony Arianto',
    'retries': 5,
    'retry_delay': timedelta(minute=2)
}

with DAG(
        dag_id='our_first_dag',
        default_args=default_args
        description='dag pertama yg gw tulis',
        startdate=datetime(2024, 4, 7, 2),
        scheduler_interval='@daily' 
    ) as dag:
    pass